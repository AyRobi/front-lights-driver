#include "uart_lin.h"

static uint8_t checksum(uint8_t const* data, uint8_t length);

#ifdef MASTER
static LinMsg_t request_msg;
#else
static LinMsg_t slave_msg;
static uint8_t is_break_received = 0;
static uint8_t is_sync_field_received = 0;
static uint16_t data_index = 0;
#endif

volatile int completed;
//Functions

//UART_Init() sets up the UART for a 8-bit data, No Parity, 1 Stop bit
//at 9600 baud with transmitter interrupts enabled
void UART_Init(void) {
    U1MODE = 0x0000;        //Clear UART1 registers
    U1STA = 0x0000;
    
    /* Configure UART mode register */
    U1MODEbits.ALTIO = 1;   //Enable alternate TX/RX pin
    U1MODEbits.PDSEL = 0;   //8 data bits, no parity
    U1MODEbits.STSEL = 0;   //1 stop bit

    /* Configure baud rate */
    U1BRG = BRGVAL;         //Load UART1 Baud Rate Generator

    /* Configure status and control register (U1STAbits) */
    /* Stays at all 0 for no interrupts */
    
    /* Remove interruptions */
    IFS0bits.U1RXIF = 0;    //Clear UART1 Receiver Interrupt Flag
    IFS0bits.U1TXIF = 0;    //Clear UART1 Transmitter Interrupt Flag
    IEC0bits.U1TXIE = 0;    //Disable transmit interruptions
    IEC0bits.U1RXIE = 0;    //Deactivate receive interruptions
    
    U1MODEbits.UARTEN = 1;  //Enable UART1 module
    U1STAbits.URXISEL = 1;  //An interrupt is generated each time a data word is transferred from the Receive Shift register (UxRSR) to the receive buffer
    U1STAbits.UTXEN = 1;    //Enable UART transmitter
}


static void UART_put_char(uint8_t c) {
    /* Wait for the transmit buffer to have space */
    while (U1STAbits.UTXBF);
    U1TXREG = c;
}

#ifndef MASTER
static uint8_t UART_get_char(void) {
    /* Wait for a character */
    while (!U1STAbits.URXDA);
    
    return U1RXREG;
}
#endif

#ifdef MASTER
void send_message(LinMsg_t const* msg) {
    uint16_t i;
    
    /* Compute the checksum */
    uint8_t sum = checksum(msg->data, msg->length);
    
    /* Send break and sync fields */
    sync_break();
    
    /* Send the PID */
    UART_put_char(msg->id);
    
    /* Send data */
    for (i = 0; i < msg->length; ++i)
        UART_put_char(msg->data[i]);
    
    /* Send the checksum */
    UART_put_char(sum);
}

int send_request(LinMsg_t const* msg) {
    /* If interruption already enabled return -1
     * since we are waiting for a slave
     */
    if (IEC0bits.U1RXIE == 1)
        return -1;
    
    /* Copy lin msg into an internal variable
     * which is shared between the IRQ and the rest of the file
     */
    request_msg = *msg;
    
    /* Reset completed status */
    completed = 0;
    
    /* Send a brake and a sync field */
    sync_break();
    
    /* Send the PID */
    UART_put_char(msg->id);
    
    /* Enable interrupts */
    IEC0bits.U1RXIE = 1;
    
    return 0;
}

int is_request_available(LinMsg_t *msg) {
    int res;
    /* Deactivate IRQ to acces shared data */
    IEC0bits.U1RXIE = 0;
    __asm__ __volatile__ ("":::"memory");
    /* If the LIN message is received 
     * we compute the checksum and test it against the received checksum
     */
    if (completed != 0) {
        *msg = request_msg;
        if (request_msg.checksum != checksum(request_msg.data, request_msg.length))
            completed = CHECKSUM_ERROR;
    }
    
    /* Create a IRQ safe copy of the shared variable */
    res = completed;
    __asm__ __volatile__ ("":::"memory");
    /* Reactivate IRQ if needed */
    IEC0bits.U1RXIE = (completed == 0);
    
    return res;
}

void sync_break() {
    /* Wait for the transmit data completion */
    while (U1STAbits.TRMT == 0);
    
    /* Enable break */
    U1STAbits.UTXBRK = 1;
    
    /* Wait at least 13 bits of break */
    __delay_us(TO_US(13));
    
    /* Stop break */
    U1STAbits.UTXBRK = 0;
    
    /* Wait for the stop bit */
    __delay_us(TO_US(1));
    
    /* Put the sync field */
    U1TXREG = 0x55;
}

static int slave_response(void) {
    static uint16_t i = 0;
    uint8_t data;
    
    /* While we can read data */
    while(U1STAbits.URXDA) {
        data = U1RXREG;
        
        /* In case of overflow error notify it */
        if (U1STAbits.OERR == 1) {
            U1STAbits.OERR = 0;
            return OVERFLOW_ERROR;
        }
        
        /* If all data are not received save it */
        if (i < request_msg.length) {
            request_msg.data[i] = data;
            ++i;
        } else { /* Else we are receiving the checksum */
            request_msg.checksum = data;
            i = 0;
            return COMPLETED;
        }       
    }
    
    return 0;
}

#else
void send_response(LinMsg_t const* msg) {
    uint16_t i;
    
    /* Compute the checksum */
    uint8_t sum = checksum(msg->data, msg->length);
    
    /* Send data */
    for (i = 0; i < msg->length; ++i)
        UART_put_char(msg->data[i]);
    
    /* Send the sum */
    UART_put_char(sum);
}

LinMsg_t wait_for_header(void) {
    
    do {
        /* Wait for a break */
        while (!U1STAbits.FERR && !U1STAbits.URXDA);

        /* Wait for a stop bit */
        while (!U1STAbits.RIDLE);
        
        /* If not a sync field go back to wait */
    } while (UART_get_char() != 0x55);
    
    /* Create the LIN message */
    LinMsg_t msg;
    msg.id = UART_get_char();
    return msg;
}

int wait_for_data(LinMsg_t *msg) {
    uint16_t i;
    
    /* Wait for all data */
    for (i = 0; i < msg->length; ++i)
        msg->data[i] = UART_get_char();
    
    /* Get the checksum */
    msg->checksum = UART_get_char();
    
    /* Compare received checksum with our checksum algorithm */
    if (msg->checksum != checksum(msg->data, msg->length))
        return CHECKSUM_ERROR;
    
    return 1;
}

/**
 * Begins a new header polling
 * @return -1 in case of error, 0 instead
 */
int start_header_receive(void) {
    /* If interruption already enabled return -1
     * since we are waiting for a slave
     */
    if (IEC0bits.U1RXIE == 1)
        return -1;
    
    /* Reset completed status */
    completed = 0;
    
    /* Reset interrupt variables */
    data_index = 0;
    is_break_received = 0;
    is_sync_field_received = 0;
    
    /* Set the base lin message */
    slave_msg.length = 8;
    
    /* Enable interrupts */
    IEC0bits.U1RXIE = 1;
    
    return 0;
}

/**
 * Checks if a message header is read
 * @param msg the message where the header will be copied
 * @return a completion code
 */
int is_header_available(LinMsg_t *msg) {
    int res;
    /* Deactivate IRQ to acces shared data */
    IEC0bits.U1RXIE = 0;
    __asm__ __volatile__ ("":::"memory");
    
    /* If the LIN header is received
     * we copy it and send it to the user
     */
    if (completed != 0) {
        *msg = slave_msg;
    }
    
    /* Create an IRQ safe copy of the shared variable */
    res = completed;
    __asm__ __volatile__ ("":::"memory");
    
    /* Reactivate IRQ if needed */
    IEC0bits.U1RXIE = completed == 0 || completed == HEADER_RECEIVED;
    
    return res;
}

/**
 * Sets the data length of the current message
 * It must be set asap after a header detection
 * @param len the new length of the LIN frame
 * @return 0 or an error code
 */
int set_data_receive_length(uint8_t len) {
    /* If interruptions are deactivated return
     * since no data are recorded
     */
    if (IEC0bits.U1RXIE == 0)
        return -1;
    
    /* Deactivate IRQ to acces shared data */
    IEC0bits.U1RXIE = 0;
    __asm__ __volatile__ ("":::"memory");
    
    /*
     * If we are not in a good state, notify the user
     */
    if (completed != HEADER_RECEIVED) {
        switch (completed) {
            case 0:
                /* Start back the inerruption since this case
                 * is reached if the header is not yet received
                 */
                IEC0bits.U1RXIE = 1; 
                return HEADER_NOT_RECEIVED_ERROR;
            case COMPLETED: /* Should not have completed yet, notify the overflow */
                return OVERFLOW_ERROR;
            default:
                return completed;
        }
    }
    
    /* Set the LIN message length */
    slave_msg.length = len;
    
    /* Enable back interrupts */
    __asm__ __volatile__ ("":::"memory");
    IEC0bits.U1RXIE = 1;
    
    return 0;
}

/**
 * Checks if data are received
 * @param msg the message where data will be stored
 * @return a completion code
 */
int is_data_received(LinMsg_t *msg) {
    int res;
    
    /* Deactivate IRQ to acces shared data */
    IEC0bits.U1RXIE = 0;
    __asm__ __volatile__ ("":::"memory");
    
    /* If the LIN data are received 
     * we compute the checksum and test it against the received checksum
     */
    if (completed != HEADER_RECEIVED && completed != 0) {
        *msg = slave_msg;
        if (slave_msg.checksum != checksum(slave_msg.data, slave_msg.length))
            completed = CHECKSUM_ERROR;
    }
    
    /* Create an IRQ safe copy of the shared variable */
    res = completed;
    __asm__ __volatile__ ("":::"memory");
    /* Reactivate IRQ if needed */
    IEC0bits.U1RXIE = (completed == HEADER_RECEIVED);
    
    return res;
}

/**
 * Stops a message reception, must be called when the master send a request
 */
void stop_data_receive(void) {
    IEC0bits.U1RXIE = 0;
}

static int master_response(void) {
    uint8_t data;
    
    /* If it is a break character */
//    if(U1STAbits.FERR && U1STAbits.URXDA) {
        is_break_received = 1;
//        return 0;
//    }
    
    /* While we can read data */
    while(U1STAbits.URXDA) {
        data = U1RXREG;
        
        /* In case of overflow error notify it */
        if (U1STAbits.OERR == 1) {
            U1STAbits.OERR = 0;
            return OVERFLOW_ERROR;
        }
        
        /* In case we are waiting for the header */
        if (completed == 0) {
            /* If we have received the break and the sync field, store the PID */
            if (is_break_received && is_sync_field_received) {
                slave_msg.id = data;
                return HEADER_RECEIVED;
            }
            
            /* If the break is received but not the sync field test it */
            if (is_break_received && !is_sync_field_received) {
                /* Check if the data is 0x55 */
                is_sync_field_received = data == 0x55;
                
                /* In case where the sync field is not good reject the break */
                is_break_received = is_sync_field_received;
            }
        } else {
            /* If all data are not received save it */
            if (data_index < slave_msg.length) {
                slave_msg.data[data_index] = data;
                ++data_index;
            } else { /* Else we are receiving the checksum */
                slave_msg.checksum = data;
                return COMPLETED;
            } 
        }      
    }
    
    return completed;
}
#endif

/*--- Calculate lin checksum ---*/
static uint8_t checksum(uint8_t const* data, uint8_t length) {
    uint8_t i;
    uint16_t check_sum = 0;

    for (i = 0; i < length; ++i)
        /* Sum all data */
        check_sum += data[i];
    
    /* 
     * Carries are in the first 8 most significant bits 
     * So we need to add it in order to have a sum with cary
     * In the case of the sum creates a new carry we need to take it
     * into account
     * 
     */
    check_sum = (check_sum >> 8) + (check_sum & 0xFF);
    check_sum = (check_sum >> 8) + (check_sum & 0xFF);

    return (uint8_t)(~check_sum);
}

#ifdef MASTER
void _ISR __attribute__((no_auto_psv)) _U1RXInterrupt(void) {

    /* Get the response Byte */
    completed = slave_response();

    /* If the LIN frame is erroned or completed 
     * deactivate IRQ
     */
    if (completed) {
        IEC0bits.U1RXIE = 0;
    }
    
    IFS0bits.U1RXIF = 0;    //Clear the UART1 transmitter interrupt flag

}
#else
void _ISR __attribute__((no_auto_psv)) _U1RXInterrupt(void) {
    EnterISR();

    /* Get the data on the uart and save it */
    completed = master_response();

    /* If the LIN frame is erroned or completed 
     * deactivate IRQ
     */
    if (completed != 0 && completed != HEADER_RECEIVED) {
        IEC0bits.U1RXIE = 0;
    }
    
    /* Set the character received flag */
    SetEvent(TASK_LIN_ID, LIN_NEW_CHARACTER_EVENT);
    
    IFS0bits.U1RXIF = 0;    //Clear the UART1 transmitter interrupt flag
    LeaveISR();
}
#endif