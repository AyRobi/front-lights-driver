#ifndef FLIGHT_H
#define	FLIGHT_H

#include "can.h"

#define THIS_NODE_ID 0x02

#define LIGHT_NODE_ID 0x51

#define BROADCAST_NODE_ID 0x00

#define FCT_PORT_A 0x10
#define FCT_PORT_B 0x11
#define FCT_PORT_C 0x12

#define ID_FCT_POS    0
#define ID_SRC_POS    8
#define ID_DEST_POS   16
#define ID_SLAVE_POS  28

#define ID_FCT_MASK   ((uint32_t) 0xFF << ID_FCT_POS)
#define ID_SRC_MASK   ((uint32_t) 0xFF << ID_SRC_POS)
#define ID_DEST_MASK  ((uint32_t) 0xFF << ID_DEST_POS)
#define ID_SLAVE_MASK ((uint32_t) 1    << ID_SLAVE_POS)

#define ID(src, dest, fct) (((uint32_t) 1      << ID_SLAVE_POS)   | \
                            ((uint32_t) (src)  << ID_SRC_POS)     | \
                            ((uint32_t) (dest) << ID_DEST_POS)    | \
                            ((uint32_t) (fct)  << ID_FCT_POS))

#define SRC(id)  ((uint32_t) ((id) & ID_SRC_MASK) >> ID_SRC_POS)
#define DEST(id) ((uint32_t) ((id) & ID_DEST_MASK) >> ID_DEST_POS)
#define FCT(id)  ((uint32_t) ((id) & ID_FCT_MASK) >> ID_FCT_POS)

#define PORT_A_RIGHT_HEAD_LIGHT_DIMMED_SET      (1 << 0)
#define PORT_A_RIGHT_HEAD_LIGHT_DIMMED_GET      (1 << 1)
#define PORT_A_LEFT_HEAD_LIGHT_DIMMED_SET       (1 << 3)
#define PORT_A_LEFT_HEAD_LIGHT_DIMMED_GET       (1 << 4)

#define PORT_B_RIGHT_BLINKER_SET                (1 << 0)
#define PORT_B_RIGHT_BLINKER_GET                (1 << 1)
#define PORT_B_RIGHT_PARKING_LIGHT_SET          (1 << 4)
#define PORT_B_RIGHT_HEAD_HIGH_BEAM_LIGHT_SET   (1 << 5)
#define PORT_B_RIGHT_LIGHT_GET                  (1 << 6)

#define PORT_C_LEFT_HEAD_HIGH_BEAM_LIGHT_SET    (1 << 0)
#define PORT_C_LEFT_LIGHT_GET                   (1 << 1)
#define PORT_C_LEFT_PARKING_LIGHT_SET           (1 << 3)
#define PORT_C_LEFT_BLINKER_GET                 (1 << 4)
#define PORT_C_LEFT_BLINKER_SET                 (1 << 5)

typedef enum {
    BLINKER_NONE = 0,
    BLINKER_LEFT = 1,
    BLINKER_RIGHT = 2,
    BLINKER_WARNING = 3
} Blinker;

typedef enum {
    HEAD_LIGHT_NONE = 0,
    HEAD_LIGHT_DIMMED = 1,
    HEAD_LIGHT_HIGH_BEAM = 2
} Head_Light;

typedef union {
    struct {
        uint8_t blinker : 2;
        uint8_t parking_light : 1;
        uint8_t head_light : 2;
        uint8_t : 3;
    };
    
    uint8_t raw;
} FLight_State;

void front_light_init(void);
void flight_on_can_frame(CAN_frame const* frame);
void set_warning(uint8_t is_warning);
void set_FLight_state(FLight_State const* state);
FLight_State get_FLight_state(void);

#endif	/* FLIGHT_H */

