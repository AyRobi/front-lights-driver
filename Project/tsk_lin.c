#include "define.h"

#include "uart_lin.h"
#include "flight.h"

#define LIN_SET_FLIGHT_PID  0x1
#define LIN_SET_WARNING_PID 0x2
#define LIN_GET_FLIGHT_PID  0x3

/**********************************************************************
 * Definition dedicated to the local functions.
 **********************************************************************/


/**********************************************************************
 * ------------------------------ TASK LIN ----------------------------
 *
 * Initialize and poll the LIN messages
 *
 **********************************************************************/
TASK(TASK_LIN) {
    int res;
    LinMsg_t lin_msg;
    
    WaitEvent(CAN_INITIALIZED_EVENT);
    ClearEvent(CAN_INITIALIZED_EVENT);
    
    UART_Init();
    
    start_header_receive();
    
    while(1) {
        WaitEvent(LIN_NEW_CHARACTER_EVENT);
        ClearEvent(LIN_NEW_CHARACTER_EVENT);
        
        res = is_header_available(&lin_msg);

        /* If a header is present and we can interpret it set the message length 
         * else forget it and wait for the next one
         */
        if (res == HEADER_RECEIVED) {
            switch (lin_msg.id) {
                case LIN_SET_FLIGHT_PID: /* Set the complete state */
                    res = set_data_receive_length(1);
                    break;
                case LIN_SET_WARNING_PID: /* Set warning monde */
                    res = set_data_receive_length(1);
                    break;
                case LIN_GET_FLIGHT_PID: /* Ask for state */
                    lin_msg.length = 1;
                    while (GetResource(FLIGHT_PORT_RES) != E_OK);
                    lin_msg.data[0] = get_FLight_state().raw;
                    ReleaseResource(FLIGHT_PORT_RES);
                    send_response(&lin_msg);
                    break;
            }

            /*
             * Here if res = 0 we are waiting for data
             */
            if (res == 0) {
                do {
                    WaitEvent(LIN_NEW_CHARACTER_EVENT);
                    ClearEvent(LIN_NEW_CHARACTER_EVENT);
                    
                    res = is_data_received(&lin_msg);
                } while (res == HEADER_RECEIVED);

                if (res == COMPLETED) {
                    switch (lin_msg.id) {
                        case LIN_SET_FLIGHT_PID:
                            set_tsk_flight_set_value(lin_msg.data[0]);
                            ActivateTask(TASK_FLIGHT_SET_ID);
                            break;
                        case LIN_SET_WARNING_PID:
                            set_tsk_flight_warn_value(lin_msg.data[0]);
                            ActivateTask(TASK_FLIGHT_WARN_ID);
                            break;
                    }
                }
            }
        }

        /* Here if res is not 0, we have an error or the message is totally read
         * so we stop the current receive and wait for the next header
         */
        if (res) {
            stop_data_receive();
            start_header_receive();
        }
    }
}
 
/* End of File : tsk_task0.c */