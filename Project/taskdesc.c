#include "define.h"

/**********************************************************************
 * --------------------- COUNTER & ALARM DEFINITION -------------------
 **********************************************************************/
volatile Counter Counter_list[] = 
  {
   /*******************************************************************
    * -------------------------- First counter ------------------------
    *******************************************************************/
   {
     {
       65535,                               /* maxAllowedValue        */
           1,                               /* ticksPerBase           */
           0                                /* minCycle               */
     },
     0,                                     /* CounterValue           */
     0                                      /* Nbr of Tick for 1 CPT  */
   }
  };

volatile Counter Counter_kernel = 
  {
    {
      65535,                               /* maxAllowedValue        */
          1,                               /* ticksPerBase           */
          0                                /* minCycle               */
    },
    0,                                     /* CounterValue           */
    0                                      /* Nbr of Tick for 1 CPT  */
  };

volatile AlarmObject Alarm_list[] = {};

#define _ALARMNUMBER_          sizeof(Alarm_list)/sizeof(AlarmObject)
#define _COUNTERNUMBER_        sizeof(Counter_list)/sizeof(Counter)
unsigned char ALARMNUMBER    = _ALARMNUMBER_;
unsigned char COUNTERNUMBER  = _COUNTERNUMBER_;
unsigned long global_counter = 0;

/**********************************************************************
 * ----------------------- RESOURCE DEFINITION ------------------------
 **********************************************************************/
volatile Resource Resource_list[] = 
  {
   /* Port ressource lock */
   {
      15,                                /* priority           */
       0,                                /* Task prio          */
       0,                                /* lock               */
   },
  };

#define _RESOURCENUMBER_       sizeof(Resource_list)/sizeof(Resource)
unsigned char RESOURCENUMBER = _RESOURCENUMBER_;


/**********************************************************************
 * ----------------------- TASK & STACK DEFINITION --------------------
 **********************************************************************/
#define DEFAULT_STACK_SIZE     128  // Warning : in 16 bits not 8 bits !
#define SMALL_STACK_SIZE       64

DeclareTask(TASK_CAN);
DeclareTask(TASK_LIN);
DeclareTask(TASK_FLIGHT_SET);
DeclareTask(TASK_FLIGHT_WARN);


u_int __attribute__((__section__(".stack_tsk_can"))) stack_can[DEFAULT_STACK_SIZE];
u_int __attribute__((__section__(".stack_tsk_lin"))) stack_lin[DEFAULT_STACK_SIZE];
u_int __attribute__((__section__(".stack_tsk_flight_set"))) stack_flight_set[SMALL_STACK_SIZE];
u_int __attribute__((__section__(".stack_tsk_flight_warn"))) stack_flight_warn[SMALL_STACK_SIZE];

/**********************************************************************
 * ---------------------- TASK DESCRIPTOR SECTION ---------------------
 **********************************************************************/
const unsigned int descromarea;
const TCB TCB_const_list[] = 
  {
   /*******************************************************************
    * ------------------------ Task CAN -------------------------------
    *******************************************************************/
   {
     SREG(stack_can),                      /* Stack_register          */
     FREG(stack_can),                      /* Frame_register          */
     TASK_CAN,                             /* StartAddress            */
     stack_can,                            /* StackAddress            */
     sizeof(stack_can),                    /* StackSize               */
     TASK_CAN_ID,                          /* TaskID                  */
     TASK_CAN_PRIO,                        /* Priority                */
     NONE,                                 /* EventWaited             */
     NONE,                                 /* EventReceived           */
     READY,                                /* State                   */
     EXTENDED,                             /* Type                    */
     0,                                    /* Time                    */ 
     0,                                    /* kernelState_copy        */ 
     NULL                                  /* next                    */	
   },
    /*******************************************************************
    * ------------------------ Task LIN -------------------------------
    *******************************************************************/
   {
     SREG(stack_lin),                      /* Stack_register          */
     FREG(stack_lin),                      /* Frame_register          */
     TASK_LIN,                             /* StartAddress            */
     stack_lin,                            /* StackAddress            */
     sizeof(stack_lin),                    /* StackSize               */
     TASK_LIN_ID,                          /* TaskID                  */
     TASK_LIN_PRIO,                        /* Priority                */
     NONE,                                 /* EventWaited             */
     NONE,                                 /* EventReceived           */
     READY,                                /* State                   */
     EXTENDED,                             /* Type                    */
     0,                                    /* Time                    */ 
     0,                                    /* kernelState_copy        */ 
     NULL                                  /* next                    */	
   },
    /*******************************************************************
    * --------------------- Task FLIGHT SET ---------------------------
    *******************************************************************/
   {
     SREG(stack_flight_set),               /* Stack_register          */
     FREG(stack_flight_set),               /* Frame_register          */
     TASK_FLIGHT_SET,                      /* StartAddress            */
     stack_flight_set,                     /* StackAddress            */
     sizeof(stack_flight_set),             /* StackSize               */
     TASK_FLIGHT_SET_ID,                   /* TaskID                  */
     TASK_FLIGHT_SET_PRIO,                 /* Priority                */
     NONE,                                 /* EventWaited             */
     NONE,                                 /* EventReceived           */
     SUSPENDED,                            /* State                   */
     EXTENDED,                             /* Type                    */
     0,                                    /* Time                    */ 
     0,                                    /* kernelState_copy        */ 
     NULL                                  /* next                    */	
   },
    /*******************************************************************
    * --------------------- Task FLIGHT SET ----------------------------
    *******************************************************************/
   {
     SREG(stack_flight_warn),              /* Stack_register          */
     FREG(stack_flight_warn),              /* Frame_register          */
     TASK_FLIGHT_WARN,                     /* StartAddress            */
     stack_flight_warn,                    /* StackAddress            */
     sizeof(stack_flight_warn),            /* StackSize               */
     TASK_FLIGHT_WARN_ID,                  /* TaskID                  */
     TASK_FLIGHT_WARN_PRIO,                /* Priority                */
     NONE,                                 /* EventWaited             */
     NONE,                                 /* EventReceived           */
     SUSPENDED,                            /* State                   */
     EXTENDED,                             /* Type                    */
     0,                                    /* Time                    */ 
     0,                                    /* kernelState_copy        */ 
     NULL                                  /* next                    */	
   },
  };

volatile unsigned int  taskdesc_addr = (unsigned int)(TCB_const_list);

#define  _TASKNUMBER_          sizeof(TCB_const_list)/sizeof(TCB)
unsigned char TASKNUMBER    = _TASKNUMBER_;
volatile TCB                   TCB_list[_TASKNUMBER_];


	
/* End of File : taskdesc.c */
