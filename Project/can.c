#include "can.h"

void dummy_callback(CAN_frame f) {
    
}

static CAN_buffer can_buffer;
static void (*read_callback)(CAN_frame) = &dummy_callback;
static uint16_t use_callback = 0;

void CAN_config(
    uint16_t ide,
    uint32_t filter_id,
    uint32_t filter_id_2,
    uint32_t filter_msk
) {
    uint16_t standard_identifier = (uint16_t)((filter_id >> 18) & 0x7FF);
    uint16_t high_identifier = (uint16_t)((filter_id >> 6) & 0xFFF);
    uint16_t low_identifier = (uint16_t)(filter_id & 0x3F);
    
    uint16_t standard_identifier_2 = (uint16_t)((filter_id_2 >> 18) & 0x7FF);
    uint16_t high_identifier_2 = (uint16_t)((filter_id_2 >> 6) & 0xFFF);
    uint16_t low_identifier_2 = (uint16_t)(filter_id_2 & 0x3F);
    
    uint16_t standard_msk = (uint16_t)((filter_msk >> 18) & 0x7FF);
    uint16_t high_msk = (uint16_t)((filter_msk >> 6) & 0xFFF); 
    uint16_t low_msk = (uint16_t)(filter_msk & 0x3F);
    
    /* Set CAN to be in configuration mode */
    C1CTRLbits.REQOP = 0x4;
    
    /* Wait for the configuration mode */
    while (C1CTRLbits.OPMODE != 0x4);
    
    /* Configure pin */
    LATFbits.LATF0 = 1; /* RF0 = C1RX set to input */
    LATFbits.LATF1 = 0; /* RF1 = C1TX set to output */
    
    /* Configure bit timing */
    C1CTRLbits.CANCKS = 1;    /* Set FCAN to FCY */
    
    C1CFG1bits.BRP = 0x1;       /* Set baud rate to 250 kBits/s */
    C1CFG1bits.SJW = 0x1;       /* Set synchronized jump to 2 TQ */
    
    C1CFG2bits.WAKFIL = 0;      /* No wake-up needed */
    C1CFG2bits.SEG2PH = 0x5;    /* 6 TQ for seg2 */
    C1CFG2bits.SEG2PHTS = 0x1;  /* Seg2 is freely programmable */
    C1CFG2bits.SAM = 0x1;       /* 3 sample points */
    C1CFG2bits.SEG1PH = 0x5;    /* 6 TQ for seg1 */
    C1CFG2bits.PRSEG = 0x6;     /* 7 TQ for propagation time */
    
    /* Configure interruption */
    C1INTF = 0; /* Clear all interrupt flags */
    C1INTEbits.RX0IE = 0x1;     /* Enable RX0 interrupts */
    C1INTEbits.TX0IE = 0x1;     /* Enable TX0 interrupts */
    IEC1bits.C1IE = 1;          /* Enable CAN interruptions */
    
    /* Configure filters and mask */
    
    /* Standard ID filter */
    C1RXF0SIDbits.SID = standard_identifier;
    C1RXF1SIDbits.SID = standard_identifier_2;
    
    /* Standard ID mask */
    C1RXM0SIDbits.SID = standard_msk;
    
    /* Mask as the same type as EXIDE */
    C1RXM0SIDbits.MIDE = 1;
    
    if (ide) {
        C1RXF0SIDbits.EXIDE = 1; /* Allow extended identifier */
        C1RXF1SIDbits.EXIDE = 1;

        /* Extended filter identifier */
        C1RXF0EIDH = high_identifier;
        C1RXF1EIDH = high_identifier_2;
        
        C1RXF0EIDLbits.EID5_0 = low_identifier;
        C1RXF1EIDLbits.EID5_0 = low_identifier_2;
        
        /* Extended mask identifier */
        C1RXM0EIDH = high_msk;
        C1RXM0EIDLbits.EID5_0 = low_msk;
    } else {
        C1RXF0SIDbits.EXIDE = 0; /* Only standard identifier */
        C1RXF1SIDbits.EXIDE = 0;
    }
    
    /* Set normal mode */
    C1CTRLbits.REQOP = 0;
    while (C1CTRLbits.OPMODE != 0);
}

int CAN_send(CAN_frame const* msg) {
    /* If a message is being transmitted return -1 */
    ClearEvent(CAN_MESSAGE_SENT);
    while (C1TX0CONbits.TXREQ) {
        WaitEvent(CAN_MESSAGE_SENT);
    }
    
    /* Setup frame type (extended or standard) */
    C1TX0SIDbits.TXIDE = msg->extended != 0;
    
    /* Setup standard ID */
    C1TX0SIDbits.SID10_6 = (uint16_t)((msg->id >> (18 + 6)) & 0x1F);
    C1TX0SIDbits.SID5_0 = (uint16_t)((msg->id >> 18) & 0x3F);

    
    if (msg->extended) {
        /* Substitute remote request */
        C1TX0SIDbits.SRR = 1;
        
        /* Setup extended ID */
        C1TX0EIDbits.EID17_14 = (uint16_t)((msg->id >> 14) & 0xF);
        C1TX0EIDbits.EID13_6 = (uint16_t)((msg->id >> 6) & 0xFF);
        C1TX0DLCbits.EID5_0 = (uint16_t) (msg->id & 0x3F);
    } else {
        /* Set the RTR (in case of standard frame TXRTR is ignored) */
        C1TX0SIDbits.SRR = msg->rtr != 0;
    }
    
    /* Set the RTR */
    C1TX0DLCbits.TXRTR = msg->rtr != 0;
    
    /* Reserved bits */
    C1TX0DLCbits.TXRB0 = 0;
    C1TX0DLCbits.TXRB1 = 0;
    
    /* Setup data length */
    C1TX0DLCbits.DLC = msg->data_length & 0xF;
    
    /* Setup data */
    C1TX0B1 = msg->data[1] << 8 | msg->data[0];
    C1TX0B2 = msg->data[3] << 8 | msg->data[2];
    C1TX0B3 = msg->data[5] << 8 | msg->data[4];
    C1TX0B4 = msg->data[7] << 8 | msg->data[6];
    
    /* Send message */
    C1TX0CONbits.TXREQ = 1;
    
    return 0;
}

void CAN_set_callback(void (*callback)(CAN_frame)) {
    read_callback = callback;
}

void CAN_use_callback(uint16_t enabled) {
    use_callback = enabled;
}

int CAN_read(CAN_frame *frame) {
    /* Deactivate interruption for this critical section */
    C1INTEbits.RX0IE = 0;
    __asm__ __volatile__ ("":::"memory");
    
    /* If the buffer is empty */
    if (!can_buffer.is_full && can_buffer.write_pos == can_buffer.read_pos) {
        C1INTEbits.RX0IE = 1;
        return -1;
    }
    
    *frame = can_buffer.buffer[can_buffer.read_pos];
    can_buffer.read_pos = (can_buffer.read_pos + 1) % BUFF_SIZE;
    can_buffer.is_full = 0;
    
    /* End of critical section */
    __asm__ __volatile__ ("":::"memory");
    C1INTEbits.RX0IE = 1;
    
    return 0;
}

static void add_to_buff(CAN_frame frame) {
    can_buffer.buffer[can_buffer.write_pos] = frame;
    can_buffer.write_pos = (can_buffer.write_pos + 1) % BUFF_SIZE;

    if (can_buffer.is_full)
        can_buffer.read_pos = can_buffer.write_pos;

    can_buffer.is_full = can_buffer.write_pos == can_buffer.read_pos;
}

void __attribute__((interrupt, no_auto_psv))_C1Interrupt(void) {
    EnterISR();
    
    if (C1INTFbits.TX0IF) {
        if (!C1TX0CONbits.TXREQ) {
            SetEvent(TASK_CAN_ID, CAN_MESSAGE_SENT);
            SetEvent(TASK_FLIGHT_SET_ID, CAN_MESSAGE_SENT);
            SetEvent(TASK_FLIGHT_WARN_ID, CAN_MESSAGE_SENT);
        }
        
        C1INTFbits.TX0IF = 0;
    }
    
    /* If interrupt fired by a message reception */
    if (C1INTFbits.RX0IF) {
        
        /* Ensure that a data can be read */
        if (C1RX0CONbits.RXFUL) {
            CAN_frame frame;
            
            /* Get the standard id */
            frame.id = (uint32_t)(C1RX0SIDbits.SID & 0x7FF) << 18;
            
            /* Check if the frame is extended with IDE bit */
            frame.extended = C1RX0SIDbits.RXIDE & 0x1;
            
            if (frame.extended) {
                /* Get the type of frame (e.g. data or request) */
                frame.rtr = C1RX0DLCbits.RXRTR & 0x1;
                
                /* Get the extended identifier */
                frame.id |= (uint32_t)(C1RX0EID & 0xFFF) << 6;
                frame.id |= (uint32_t)(C1RX0DLCbits.EID5_0 & 0x3F);
            } else {
                /*
                 * In case of a standard frame the RTR bit is shadowed 
                 * by the SRR bit
                 */
                frame.rtr = C1RX0SIDbits.SRR & 0x1;
            }
            
            /* Get the DLC */
            frame.data_length = C1RX0DLCbits.DLC & 0xF;
            
            /* Get all data from data fields */
            frame.data[0] = C1RX0B1 & 0xFF;
            frame.data[1] = (C1RX0B1 >> 8) & 0xFF;
            frame.data[2] = C1RX0B2 & 0xFF;
            frame.data[3] = (C1RX0B2 >> 8) & 0xFF;
            frame.data[4] = C1RX0B3 & 0xFF;
            frame.data[5] = (C1RX0B3 >> 8) & 0xFF;
            frame.data[6] = C1RX0B4 & 0xFF;
            frame.data[7] = (C1RX0B4 >> 8) & 0xFF;

            if (use_callback) {
                /* Call the user function*/
                (*read_callback)(frame);
            } else {
                /* Add read frame to circular buffer */
                add_to_buff(frame);
            }
            
            /* Set the event for the can task */
            SetEvent(TASK_CAN_ID, CAN_NEW_FRAME_EVENT);

            /* Indicate that the frame is read to the CAN module */
            C1RX0CONbits.RXFUL = 0;
        }
        
        /* Clear interrupt flag */
        C1INTFbits.RX0IF = 0;
    }
    
    // Clear CAN1 interrupt flag
    IFS1bits.C1IF = 0;
    
    LeaveISR();
}