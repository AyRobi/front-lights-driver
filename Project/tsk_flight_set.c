#include "define.h"
#include "flight.h"


/**********************************************************************
 * Definition dedicated to the local functions.
 **********************************************************************/
static uint16_t flight_set = 0;

void set_tsk_flight_set_value(uint16_t new_val) {
    flight_set = new_val;
}

/**********************************************************************
 * --------------------------- TASK FLIGHT LIN ------------------------
 *
 * Acts according to the lin message
 *
 **********************************************************************/
TASK(TASK_FLIGHT_SET) {
    FLight_State state;
    state.raw = (uint8_t)flight_set;
    while (GetResource(FLIGHT_PORT_RES) != E_OK);
    set_FLight_state(&state);
    ReleaseResource(FLIGHT_PORT_RES);
    TerminateTask();
}
 
/* End of File : tsk_task0.c */