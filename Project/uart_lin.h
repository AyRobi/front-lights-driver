/* 
 * File:   lin.h
 * Author: asuer
 *
 * Created on 22 avril 2020, 16:56
 */

#ifndef LIN_H
#define	LIN_H

#define FCY 20000000UL
    
#include <xc.h>
#include <libpic30.h>
#include <stdint.h>
#include "define.h"

#define BAUDRATE        9600                    //Desired Baud Rate
#define BRGVAL          ((FCY/BAUDRATE)/16)-1   //Formula for U1BRG register
                                                //from dsPIC30F Family
                                                //Reference Manual
#define TO_US(X) ((X / BAUDRATE) * 1000000UL + 1)

#define HEADER_RECEIVED 2
#define COMPLETED 1

#define OVERFLOW_ERROR -2
#define CHECKSUM_ERROR -3
#define HEADER_NOT_RECEIVED_ERROR -4

//#define MASTER

typedef struct {
    uint8_t id;
    uint8_t length;
    uint8_t checksum; /* Only useful for received frame */
    uint8_t data[8];
} LinMsg_t;

void UART_Init(void);

#ifdef MASTER
void send_message(LinMsg_t const* msg);
int send_request(LinMsg_t const* msg);
int is_request_available(LinMsg_t *msg);
void sync_break();
#else
void send_response(LinMsg_t const* msg);
LinMsg_t wait_for_header(void);
int wait_for_data(LinMsg_t *msg);
int start_header_receive(void);
int is_header_available(LinMsg_t *msg);
int set_data_receive_length(uint8_t len);
int is_data_received(LinMsg_t *msg);
void stop_data_receive(void);
#endif

void __attribute__((__interrupt__)) _U1RXInterrupt(void);

#endif	/* LIN_H */

