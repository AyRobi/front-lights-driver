/* 
 * File:   can.h
 * Author: asuer
 *
 * Created on 2 avril 2020, 16:25
 */

#ifndef CAN_H
#define	CAN_H

#include <xc.h>
#include <stdint.h>
#include "define.h"

#define FCY 20000000UL
#define BUFF_SIZE 16

typedef struct {
    uint32_t id;
    uint8_t data_length;
    uint8_t rtr;
    uint8_t extended;
    uint8_t data[8];
} CAN_frame;

typedef struct {
    uint8_t read_pos;
    uint8_t write_pos;
    uint8_t is_full;
    CAN_frame buffer[BUFF_SIZE];
} CAN_buffer;

void CAN_config(
    uint16_t ide,
    uint32_t filter_id,
    uint32_t filter_id_2,
    uint32_t filter_msk
);

void CAN_set_callback(void (*callback)(CAN_frame));
void CAN_use_callback(uint16_t enabled);
int CAN_read(CAN_frame *frame);
int CAN_send(CAN_frame const* msg);

void __attribute__((interrupt, no_auto_psv))_C1Interrupt(void);

#endif	/* CAN_H */

