# Front lights driver

## Description
This simple project is aimed to show the implementation of a CAN and a LIN driver to control car front lights. The latter is controlled by using a CAN bus while the integrated circuit (here a test board with a dsPIC30F4011) communicates with its master through the LIN protocol. The whole tasks are scheduled by a real time operating system, here PICOS.

## Technologies
Following technologies are used in this project:
* C
* CAN
* LIN
* PICOS

## Authors
* ROBITAILLE Aymeric (AyRobi)