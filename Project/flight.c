#include "flight.h"

static uint8_t port_A;
static uint8_t port_B;
static uint8_t port_C;

static FLight_State commodo_state;
static uint8_t _is_warning;

static uint8_t wait_until_rep(uint8_t function);

/**
 * Must be called from CAN task
 */
void front_light_init(void) {
    CAN_config(
            1, 
            ID(LIGHT_NODE_ID, THIS_NODE_ID, FCT_PORT_A),
            ID(BROADCAST_NODE_ID, LIGHT_NODE_ID, FCT_PORT_A),
            (uint32_t) 0x11110010
     );
    
    /* Get information from front lights */
    uint8_t PA = wait_until_rep(FCT_PORT_A);
    uint8_t PB = wait_until_rep(FCT_PORT_B);
    uint8_t PC = wait_until_rep(FCT_PORT_C);
    
    /* Translate out to in entries */
    
    /* 
     * Since we cannot differenciate parking and dimmed light from control
     * We assumes that the light is parking, and set it to be sure
     */
    
    port_A = 0;
    port_B = 0;
    port_C = 0;
    
    if (PA & PORT_A_RIGHT_HEAD_LIGHT_DIMMED_GET)
        port_A |= PORT_A_RIGHT_HEAD_LIGHT_DIMMED_SET;
    
    if (PA & PORT_A_LEFT_HEAD_LIGHT_DIMMED_GET)
        port_A |= PORT_A_LEFT_HEAD_LIGHT_DIMMED_SET;
    
    if (PB & PORT_B_RIGHT_BLINKER_GET)
        port_B |= PORT_B_RIGHT_BLINKER_SET;
    
    if (PB & PORT_B_RIGHT_LIGHT_GET)
        port_B |= PORT_B_RIGHT_PARKING_LIGHT_SET;
    
    if (PC & PORT_C_LEFT_BLINKER_GET)
        port_C |= PORT_C_LEFT_BLINKER_SET;
    
    if (PC & PORT_C_LEFT_LIGHT_GET)
        port_C |= PORT_C_LEFT_PARKING_LIGHT_SET;
    

    commodo_state = get_FLight_state();
    _is_warning = commodo_state.blinker == BLINKER_WARNING;
    set_FLight_state(&commodo_state);
}

void set_warning(uint8_t is_warning) {
    _is_warning = is_warning;
    set_FLight_state(&commodo_state);
}

void set_FLight_state(FLight_State const* state) {
    uint8_t PA = 0;
    uint8_t PB = 0;
    uint8_t PC = 0;
    
    commodo_state = *state;
    
    Blinker blinker = state->blinker;
    if (_is_warning)
        blinker = BLINKER_WARNING;
    
    switch (blinker) {
        case BLINKER_LEFT:
            PC |= PORT_C_LEFT_BLINKER_SET;
            break;
        case BLINKER_RIGHT:
            PB |= PORT_B_RIGHT_BLINKER_SET;
            break;
        case BLINKER_WARNING:
            PC |= PORT_C_LEFT_BLINKER_SET;
            PB |= PORT_B_RIGHT_BLINKER_SET;
            break;
        default:
            break;
    }
    
    if (state->parking_light) {
        PB |= PORT_B_RIGHT_PARKING_LIGHT_SET;
        PC |= PORT_C_LEFT_PARKING_LIGHT_SET;
    }
    
    switch (state->head_light) {
        case HEAD_LIGHT_DIMMED:
            PA |= PORT_A_LEFT_HEAD_LIGHT_DIMMED_SET |
                    PORT_A_RIGHT_HEAD_LIGHT_DIMMED_SET;
            break;
        case HEAD_LIGHT_HIGH_BEAM:
            PC |= PORT_C_LEFT_HEAD_HIGH_BEAM_LIGHT_SET;
            PB |= PORT_B_RIGHT_HEAD_HIGH_BEAM_LIGHT_SET;
            break;
        default:
            break;
    }
    
    CAN_frame frame = {
        .id = ID(THIS_NODE_ID, LIGHT_NODE_ID, FCT_PORT_A),
        .extended = 1,
        .rtr = 0,
        .data_length = 1,
        .data[0] = PA
    };
    
    CAN_send(&frame);
    
    frame.id = ID(THIS_NODE_ID, LIGHT_NODE_ID, FCT_PORT_B);
    frame.data[0] = PB;
    CAN_send(&frame);
    
    frame.id = ID(THIS_NODE_ID, LIGHT_NODE_ID, FCT_PORT_C);
    frame.data[0] = PC;
    CAN_send(&frame);
    
    port_A = PA;
    port_B = PB;
    port_C = PC;
}

FLight_State get_FLight_state(void) {
    FLight_State state;
    state.raw = 0;
    
    uint16_t is_left_blink = port_C & PORT_C_LEFT_BLINKER_SET;
    uint16_t is_right_blink = port_B & PORT_B_RIGHT_BLINKER_SET;
    if (is_left_blink && is_right_blink)
        state.blinker = BLINKER_WARNING;
    else if (is_left_blink)
        state.blinker = BLINKER_LEFT;
    else if (is_right_blink)
        state.blinker = BLINKER_RIGHT;
    else
        state.blinker = BLINKER_NONE;
    
    /* If one parking light is on consider both as on */
    state.parking_light = (port_B & PORT_B_RIGHT_PARKING_LIGHT_SET) ||
            (port_C & PORT_C_LEFT_PARKING_LIGHT_SET);
    
    uint16_t has_high_beam = (port_B & PORT_B_RIGHT_HEAD_HIGH_BEAM_LIGHT_SET) ||
            (port_C & PORT_C_LEFT_HEAD_HIGH_BEAM_LIGHT_SET);
    
    uint16_t has_dimmed_beam = (port_A & PORT_A_RIGHT_HEAD_LIGHT_DIMMED_SET) ||
            (port_A & PORT_A_LEFT_HEAD_LIGHT_DIMMED_SET);
    
    /* Get the head light with the following priority : high beam > dimmed light > None */
    if (has_high_beam)
        state.head_light = HEAD_LIGHT_HIGH_BEAM;
    else if (has_dimmed_beam)
        state.head_light = HEAD_LIGHT_DIMMED;
    else
        state.head_light = HEAD_LIGHT_NONE;
    
    return state;
}

void flight_on_can_frame(CAN_frame const* frame) {
        /* Do not accept requests and message with a DLC different of 1 */
        if (frame->rtr || frame->data_length != 1)
            return;
        
        /* Listen only control frame */
        if (DEST(frame->id) != LIGHT_NODE_ID)
            return;
        
        /* Update port values if a control frame is sent by another component */
        uint16_t port = FCT(frame->id);
        uint8_t data = frame->data[0];
        switch (port) {
            case FCT_PORT_A:
                port_A = data;
                break;
            case FCT_PORT_B:
                port_B = data;
                break;
            case FCT_PORT_C:
                port_C = data;
                break;
        }
}

static uint8_t wait_until_rep(uint8_t function) {
    uint32_t response_id = ID(LIGHT_NODE_ID, THIS_NODE_ID, function);
    
    CAN_frame frame = {
        .id = ID(THIS_NODE_ID, LIGHT_NODE_ID, function),
        .rtr = 1,
        .extended = 1
    };
    
    CAN_send(&frame);
    
    do {
        WaitEvent(CAN_NEW_FRAME_EVENT);
        ClearEvent(CAN_NEW_FRAME_EVENT);
        
        CAN_read(&frame);
    } while (frame.id !=  response_id);
    
    return frame.data[0];
}