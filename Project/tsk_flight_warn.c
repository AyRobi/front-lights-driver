#include "define.h"
#include "flight.h"


/**********************************************************************
 * Definition dedicated to the local functions.
 **********************************************************************/
static uint16_t flight_warn = 0;

void set_tsk_flight_warn_value(uint16_t new_val) {
    flight_warn = new_val;
}

/**********************************************************************
 * --------------------------- TASK FLIGHT LIN ------------------------
 *
 * Acts according to the lin message
 *
 **********************************************************************/
TASK(TASK_FLIGHT_WARN) {
    uint8_t state = (uint8_t) flight_warn;
    while (GetResource(FLIGHT_PORT_RES) != E_OK);
    set_warning(state);
    ReleaseResource(FLIGHT_PORT_RES);
    TerminateTask();
}
 
/* End of File : tsk_task0.c */