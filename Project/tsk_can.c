#include "define.h"

#include "flight.h"
#include "can.h"

/**********************************************************************
 * Definition dedicated to the local functions.
 **********************************************************************/


/**********************************************************************
 * ------------------------------ TASK CAN ----------------------------
 *
 * CAN task, initialize and poll for new can frame
 *
 **********************************************************************/
TASK(TASK_CAN) {
    CAN_frame frame;
    
    front_light_init();

    SetEvent(TASK_LIN_ID, CAN_INITIALIZED_EVENT);
    
    while (1) {
        WaitEvent(CAN_NEW_FRAME_EVENT);
        ClearEvent(CAN_NEW_FRAME_EVENT);

        while (GetResource(FLIGHT_PORT_RES) != E_OK);
        
        while (CAN_read(&frame) == 0)
            flight_on_can_frame(&frame);
        
        ReleaseResource(FLIGHT_PORT_RES);
    }
}
 
/* End of File : tsk_task0.c */